/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   path_finding.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/02 12:53:15 by tstephen          #+#    #+#             */
/*   Updated: 2018/09/03 00:43:41 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PATH_FINDING_H
# define PATH_FINDING_H
# include "lem_in.h"

t_path						*ft_new_choice(int index);
void						ft_path_push_back(t_path **head, t_path *node);
void						ft_pop_path(t_path **path);
void						ft_build_room_indices(t_rooms *rooms);
int							ft_path_size(t_path *head);
t_path						*ft_pathfind(t_game *game);
t_path						*ft_goto_top(t_path *head);

#endif
