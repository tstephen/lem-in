/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/22 14:48:45 by tstephen          #+#    #+#             */
/*   Updated: 2018/09/03 15:06:11 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H
# define KNRM  "\x1B[0m"
# define KRED  "\x1B[31m"
# define KGRN  "\x1B[32m"
# define KYEL  "\x1B[33m"
# define KBLU  "\x1B[34m"
# define KMAG  "\x1B[35m"
# define KCYN  "\x1B[36m"
# define KWHT  "\x1B[37m"
# include "../libft/libft.h"

typedef struct				s_input
{
	char					*line;
	struct s_input			*next;
}							t_input;

typedef struct				s_rooms
{
	char					*room;
	int						index;
	struct s_rooms			*next;
}							t_rooms;

typedef struct				s_path
{
	int						index;
	struct s_path			*next;
}							t_path;

typedef struct				s_game
{
	t_input					*input;
	t_rooms					*rooms;
	t_rooms					*start;
	t_rooms					*end;
	int						**links_matrix;
	int						matrix_dimensions;
	size_t					ants;
	t_path					*paths;
}							t_game;

t_input						*ft_new_node(char *line);
void						ft_input_push_back(t_input **head, t_input *node);
void						ft_store_input(t_game **game);
void						ft_input_del(t_input **input);
void						ft_store_input_helper(t_game **game, int *i,
												char *line);
void						ft_switch(int *i, char *line);

t_game						*ft_new_game_struct();
t_rooms						*ft_new_room(char *line);
void						ft_room_push_back(t_rooms **head, t_rooms *room);
void						ft_setup_links_matrix(t_game *game);
int							ft_num_rooms(t_rooms *rooms);
void						ft_game_del(t_game **game);
void						ft_matrix_del(int ***arr, int size);
void						ft_path_del(t_path **head);

void						ft_std_error(t_game **game, char *str,
										char *colour);
void						ft_error_on_ants(t_game **game, char *line);
int							ft_is_valid_room(t_game *game, char *str);
int							ft_is_duplicate_room(t_game *game, char *str);
int							ft_find_start_or_end(t_game **game, t_input **input,
												char *line);
void						ft_find_start_or_end_helper(t_game **game,
								t_input **input, char *new_line, char *line);
void						ft_duplicate_start_end(t_game **game, int i);
int							ft_is_a_room(t_rooms *rooms, char *str);
void						ft_is_valid_link(t_game **game, char *line);
t_rooms						*ft_last_room_added(t_rooms *rooms);

void						ft_print_input(t_input *input);

#endif
