/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game_handler.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/22 23:13:11 by tstephen          #+#    #+#             */
/*   Updated: 2018/09/03 00:40:39 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/lem_in.h"

int			ft_num_rooms(t_rooms *rooms)
{
	int		i;

	i = 0;
	while (rooms)
	{
		i++;
		rooms = rooms->next;
	}
	return (i);
}

void		ft_setup_links_matrix(t_game *game)
{
	int		j;
	int		k;

	j = 0;
	game->matrix_dimensions = ft_num_rooms(game->rooms);
	game->links_matrix = (int**)ft_memalloc(sizeof(int*) *
						game->matrix_dimensions);
	while (j < game->matrix_dimensions)
	{
		k = 0;
		game->links_matrix[j] = (int*)ft_memalloc(sizeof(int) *
								game->matrix_dimensions);
		while (k < game->matrix_dimensions)
			game->links_matrix[j][k++] = 0;
		j++;
	}
}

t_game		*ft_new_game_struct(void)
{
	t_game		*game;

	game = (t_game*)ft_memalloc(sizeof(t_game));
	game->ants = 0;
	game->end = NULL;
	game->start = NULL;
	game->rooms = NULL;
	game->input = NULL;
	game->links_matrix = NULL;
	game->paths = NULL;
	game->matrix_dimensions = 0;
	return (game);
}

static void	ft_rooms_del(t_rooms **rooms)
{
	t_rooms		*current;

	current = *rooms;
	if (current)
		current = current->next;
	while (*rooms)
	{
		free((*rooms)->room);
		free(*rooms);
		*rooms = current;
		if (current)
			current = current->next;
	}
}

void		ft_game_del(t_game **game)
{
	ft_input_del(&(*game)->input);
	ft_rooms_del(&((*game)->rooms));
	ft_matrix_del(&((*game)->links_matrix), (*game)->matrix_dimensions);
	ft_path_del(&((*game)->paths));
	free(*game);
	*game = NULL;
}
