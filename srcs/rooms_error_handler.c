/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rooms_error_handler.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/24 00:34:53 by tstephen          #+#    #+#             */
/*   Updated: 2018/09/03 14:34:36 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/lem_in.h"

static int		ft_has_dash(char *str)
{
	while (*str)
	{
		if (*str == ' ')
			break ;
		if (*str == '-')
			return (1);
		str++;
	}
	return (0);
}

static int		ft_is_valid_room_helper(t_game *game, char *str,
										char *first_space)
{
	char	*room_name;
	char	*second_space;

	second_space = ft_strchr(first_space, ' ');
	if (second_space)
		second_space++;
	if (!second_space && !(*second_space))
		return (0);
	if (!ft_isdigit(*second_space))
		return (0);
	else
	{
		while (ft_isdigit(*second_space))
			second_space++;
		if (*second_space)
			return (0);
	}
	room_name = ft_strndup(str, ft_strchr(str, ' ') - str);
	if (ft_is_duplicate_room(game, room_name))
	{
		free(room_name);
		return (2);
	}
	ft_room_push_back(&(game->rooms), ft_new_room(room_name));
	return (1);
}

int				ft_is_valid_room(t_game *game, char *str)
{
	char	*first_space;
	int		check;

	if (ft_has_dash(str))
		return (0);
	if (*str == ' ')
		return (0);
	first_space = ft_strchr(str, ' ');
	if (first_space)
		first_space++;
	if (!first_space || !(*first_space))
		return (0);
	if (!ft_isdigit(*first_space))
		return (0);
	else
	{
		while (ft_isdigit(*first_space))
			first_space++;
		if (*first_space != ' ')
			return (0);
	}
	check = ft_is_valid_room_helper(game, str, first_space);
	return (check);
}

int				ft_is_duplicate_room(t_game *game, char *str)
{
	t_rooms *current;

	current = game->rooms;
	while (current)
	{
		if (!ft_strcmp(current->room, str))
			return (1);
		current = current->next;
	}
	return (0);
}
