/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utility.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/22 15:04:24 by tstephen          #+#    #+#             */
/*   Updated: 2018/09/03 14:52:25 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/path_finding.h"

void		ft_print_input(t_input *input)
{
	while (input)
	{
		ft_putendl(input->line);
		input = input->next;
	}
	ft_putchar('\n');
}

void		ft_input_del(t_input **input)
{
	t_input		*current;

	current = *input;
	if (current)
		current = current->next;
	while (*input)
	{
		free((*input)->line);
		free(*input);
		*input = current;
		if (current)
			current = current->next;
	}
	*input = NULL;
}

void		ft_matrix_del(int ***arr, int size)
{
	int		**matrix;
	int		i;

	matrix = *arr;
	if (!matrix)
		return ;
	i = 0;
	while (i < size)
	{
		free(matrix[i]);
		i++;
	}
	free(matrix);
	*arr = NULL;
}

void		ft_path_del(t_path **head)
{
	t_path *current;

	current = *head;
	if (!current)
		return ;
	while (current)
	{
		current = current->next;
		free(*head);
		*head = current;
	}
}

void		ft_switch(int *i, char *line)
{
	if (!ft_strcmp("##start", line) && *i != 2)
		*i = 3;
	else if (!ft_strcmp("##end", line) && *i != 2)
		*i = 4;
	else if (!ft_strncmp("#", line, 1))
		*i = -1;
	else if (ft_strchr(line, ' ') && *i != 2)
		*i = 1;
	else if (ft_strlen(line) == 0)
		*i = 5;
	else if (ft_strchr(line, '-'))
		*i = 2;
	else
		*i = 0;
}
