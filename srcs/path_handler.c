/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   path_handler.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/02 13:01:30 by tstephen          #+#    #+#             */
/*   Updated: 2018/09/03 14:37:20 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/path_finding.h"

void			ft_build_room_indices(t_rooms *rooms)
{
	int		i;

	i = 0;
	while (rooms)
	{
		rooms->index = i;
		i++;
		rooms = rooms->next;
	}
}

t_path			*ft_new_choice(int index)
{
	t_path		*node;

	node = (t_path*)ft_memalloc(sizeof(t_path));
	node->index = index;
	node->next = NULL;
	return (node);
}

void			ft_path_push_back(t_path **head, t_path *node)
{
	t_path *current;

	current = *head;
	if (!current)
		*head = node;
	else
	{
		while (current->next)
			current = current->next;
		current->next = node;
	}
}

void			ft_pop_path(t_path **head)
{
	t_path *current;

	current = *head;
	if (!(*head))
		return ;
	if (!(current->next))
	{
		free(current);
		*head = NULL;
		return ;
	}
	while (current->next->next)
		current = current->next;
	free(current->next);
	current->next = NULL;
}

int				ft_path_size(t_path *head)
{
	int	i;

	i = 0;
	while (head)
	{
		head = head->next;
		i++;
	}
	return (i);
}
