/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_handler.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/23 10:56:57 by tstephen          #+#    #+#             */
/*   Updated: 2018/08/30 16:39:43 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/lem_in.h"

void		ft_std_error(t_game **game, char *str, char *colour)
{
	ft_putstr_fd("\e[0;91;4;1m", 2);
	ft_putstr_fd("ERROR", 2);
	ft_putstr_fd(KNRM, 2);
	ft_putstr_fd(" : ", 2);
	ft_putstr_fd(colour, 2);
	ft_putendl_fd(str, 2);
	ft_putstr_fd(KNRM, 2);
	ft_game_del(game);
	exit(0);
}
