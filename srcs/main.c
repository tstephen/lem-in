/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/22 15:03:15 by tstephen          #+#    #+#             */
/*   Updated: 2018/09/03 14:24:56 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/path_finding.h"

static t_rooms	*ft_match_index(t_rooms *room, int index)
{
	while (room)
	{
		if (room->index == index)
			return (room);
		room = room->next;
	}
	return (NULL);
}

static void		ft_send_ants(t_game *game, t_path *head)
{
	size_t		i;
	t_path		*current;

	i = 1;
	if (head->index == game->end->index)
		return ;
	while (i <= game->ants)
	{
		current = head->next;
		while (current)
		{
			ft_putstr("L");
			ft_putnbr(i);
			ft_putstr("-");
			ft_putendl(ft_match_index(game->rooms, current->index)->room);
			current = current->next;
		}
		i++;
	}
}

int				main(void)
{
	t_game		*game;

	game = ft_new_game_struct();
	ft_store_input(&game);
	ft_build_room_indices(game->rooms);
	game->paths = ft_pathfind(game);
	if (ft_goto_top(game->paths)->index != game->end->index)
		ft_std_error(&game, "no path to end", KYEL);
	ft_print_input(game->input);
	ft_send_ants(game, game->paths);
	ft_game_del(&game);
	return (0);
}
