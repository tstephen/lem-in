/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   path_finding.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/02 12:52:16 by tstephen          #+#    #+#             */
/*   Updated: 2018/09/03 14:39:44 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/path_finding.h"

static int		ft_top_path(t_path *head)
{
	if (!head)
		return (-1);
	while (head->next)
		head = head->next;
	return (head->index);
}

t_path			*ft_goto_top(t_path *head)
{
	if (!head)
		return (NULL);
	while (head->next)
		head = head->next;
	return (head);
}

static void		ft_flip_existing_paths(t_game *game, t_path *head)
{
	t_path	*current;

	current = ft_goto_top(head);
	while (head)
	{
		game->links_matrix[current->index][head->index] = 0;
		game->links_matrix[head->index][current->index] = 0;
		head = head->next;
	}
}

static int		ft_take_first_link(t_game *game, t_path *head)
{
	int		i;

	i = 0;
	ft_flip_existing_paths(game, head);
	head = ft_goto_top(head);
	if (game->links_matrix[head->index][game->end->index] == 1)
	{
		game->links_matrix[head->index][game->end->index] = 0;
		game->links_matrix[game->end->index][head->index] = 0;
		return (game->end->index);
	}
	while (i < game->matrix_dimensions)
	{
		if (game->links_matrix[head->index][i] == 1)
		{
			game->links_matrix[head->index][i] = 0;
			game->links_matrix[i][head->index] = 0;
			return (i);
		}
		i++;
	}
	return (-1);
}

t_path			*ft_pathfind(t_game *game)
{
	t_path	*head;
	int		go;

	head = NULL;
	ft_path_push_back(&head, ft_new_choice(game->start->index));
	while (ft_top_path(head) != game->end->index)
	{
		if ((go = ft_take_first_link(game, head)) != -1)
			ft_path_push_back(&head, ft_new_choice(go));
		else if (ft_path_size(head) != 1)
			ft_pop_path(&head);
		else
			break ;
	}
	return (head);
}
