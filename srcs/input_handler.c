/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input_handler.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/22 14:55:01 by tstephen          #+#    #+#             */
/*   Updated: 2018/09/03 14:52:21 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/lem_in.h"

t_input		*ft_new_node(char *line)
{
	t_input		*new_node;

	new_node = (t_input*)ft_memalloc(sizeof(t_input));
	new_node->next = NULL;
	new_node->line = line;
	return (new_node);
}

void		ft_input_push_back(t_input **head, t_input *node)
{
	t_input		*current;

	current = *head;
	if (!(*head))
		*head = node;
	else
	{
		while (current->next)
			current = current->next;
		current->next = node;
	}
}

static void	ft_validate_line(t_game **game, char *line, t_input **input, int i)
{
	int		check;

	if (i == 1)
	{
		if (!(check = ft_is_valid_room(*game, line)))
			ft_std_error(game, "rooms not formatted correctly", KYEL);
		if (check == 2)
			ft_std_error(game, "duplicate room", KYEL);
		if ((*game)->links_matrix)
			ft_std_error(game, "room in incorrect position", KYEL);
	}
	else if (i == -1)
		return ;
	else if (i == 3 || i == 4)
		ft_find_start_or_end(game, input, line);
	else if (i == 5)
		ft_std_error(game, "multiple newlines", KYEL);
	else if (i == 0)
		ft_std_error(game, "error on input", KRED);
	else if (i == 2)
	{
		if (!(*game)->links_matrix)
			ft_setup_links_matrix(*game);
		ft_is_valid_link(game, line);
	}
}

void		ft_store_input_helper(t_game **game, int *i, char *line)
{
	t_input		*new_node;

	ft_switch(i, line);
	if (*i == 2)
		if (!(*game)->end || !(*game)->start)
		{
			free(line);
			ft_std_error(game, "no start/end provided before links", KYEL);
		}
	new_node = ft_new_node(line);
	ft_input_push_back(&(*game)->input, new_node);
	ft_duplicate_start_end(game, *i);
	ft_validate_line(game, line, &(*game)->input, *i);
}

void		ft_store_input(t_game **game)
{
	char		*line;
	t_input		*new_node;
	int			i;

	i = 0;
	while (get_next_line(0, &line) && !ft_strncmp(line, "#", 1))
	{
		if (!ft_strcmp(line, "##start") || !ft_strcmp(line, "##end"))
			break ;
		new_node = ft_new_node(line);
		ft_input_push_back(&(*game)->input, new_node);
	}
	ft_error_on_ants(game, line);
	new_node = ft_new_node(line);
	ft_input_push_back(&(*game)->input, new_node);
	while (get_next_line(0, &line))
		ft_store_input_helper(game, &i, line);
	if (line)
		free(line);
	if (!((*game)->links_matrix))
		ft_std_error(game, "insufficent data", KYEL);
}
