/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_rooms.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/22 23:00:59 by tstephen          #+#    #+#             */
/*   Updated: 2018/09/03 15:03:01 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/lem_in.h"

t_rooms		*ft_new_room(char *line)
{
	t_rooms		*room;

	room = (t_rooms*)ft_memalloc(sizeof(t_rooms));
	room->room = line;
	room->index = 0;
	room->next = NULL;
	return (room);
}

void		ft_room_push_back(t_rooms **head, t_rooms *room)
{
	t_rooms		*current;

	current = *head;
	if (!(*head))
		*head = room;
	else
	{
		while (current->next)
			current = current->next;
		current->next = room;
	}
}

t_rooms		*ft_last_room_added(t_rooms *rooms)
{
	while (rooms->next)
		rooms = rooms->next;
	return (rooms);
}

int			ft_find_start_or_end(t_game **game, t_input **input, char *line)
{
	char	*new_line;

	while (get_next_line(0, &new_line))
	{
		if (!ft_strcmp(new_line, "##start") || !ft_strcmp(new_line, "##end"))
			ft_std_error(game, "start/end must be followed by a room", KYEL);
		else if (!ft_strncmp(new_line, "#", 1))
			ft_input_push_back(input, ft_new_node(new_line));
		else
			break ;
		new_line = NULL;
	}
	ft_find_start_or_end_helper(game, input, new_line, line);
	return (1);
}

void		ft_duplicate_start_end(t_game **game, int i)
{
	if (i == 3 && (*game)->start)
		ft_std_error(game, "duplicate start", KYEL);
	if (i == 4 && (*game)->end)
		ft_std_error(game, "duplicate end", KYEL);
}
