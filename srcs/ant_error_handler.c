/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ant_error_handler.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/23 10:54:12 by tstephen          #+#    #+#             */
/*   Updated: 2018/09/03 14:44:01 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/lem_in.h"

static int	ft_only_numbers(char *str)
{
	int		i;

	i = 0;
	while (*str)
		if (ft_isdigit(*str))
		{
			if (*(++str) != '0')
				i++;
		}
		else if (*str == '-')
		{
			if ((str - 1))
			{
				if (ft_isdigit(*(str - 1)))
					return (-1);
			}
			if (!(str + 1))
				return (-1);
			str++;
		}
		else
			return (-1);
	if (*str)
		return (-1);
	return (i);
}

static void	ft_error_on_ants_helper(t_game **game, char *line, int num_ants)
{
	if (num_ants <= 0)
	{
		free(line);
		ft_std_error(game, "number of ants cannot be negative OR equal to zero",
					KYEL);
	}
	if (num_ants > INT_MAX)
	{
		free(line);
		ft_std_error(game, "number of ants cannot be greater than INT_MAX",
					KYEL);
	}
}

void		ft_error_on_ants(t_game **game, char *line)
{
	long long	num_ants;

	num_ants = 1;
	if (!line)
	{
		ft_game_del(game);
		exit(0);
	}
	if ((num_ants = ft_only_numbers(line)) == -1 || num_ants == 0)
	{
		free(line);
		ft_std_error(game, "number of ants not formatted correctly", KYEL);
	}
	if (num_ants > 10)
		num_ants = *line == '-' ? INT_MIN : 2147483648;
	else
		num_ants = ft_atoll(line);
	ft_error_on_ants_helper(game, line, num_ants);
	(*game)->ants = (size_t)num_ants;
}
