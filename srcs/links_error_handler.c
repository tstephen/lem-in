/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   links_error_handler.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/30 11:20:58 by tstephen          #+#    #+#             */
/*   Updated: 2018/09/03 15:05:24 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/lem_in.h"

int			ft_is_a_room(t_rooms *rooms, char *str)
{
	while (rooms)
	{
		if (!ft_strcmp(rooms->room, str))
			return (1);
		rooms = rooms->next;
	}
	return (0);
}

int			ft_find_room_index(t_rooms *rooms, char *str)
{
	int		i;

	i = 0;
	while (rooms)
	{
		if (!ft_strcmp(rooms->room, str))
			return (i);
		i++;
		rooms = rooms->next;
	}
	return (-1);
}

void		ft_make_link(t_game *game, char *first, char *second)
{
	int		index_first;
	int		index_second;

	index_first = ft_find_room_index(game->rooms, first);
	index_second = ft_find_room_index(game->rooms, second);
	if (index_first == -1 || index_second == -1)
		return ;
	game->links_matrix[index_first][index_second] = 1;
	game->links_matrix[index_second][index_first] = 1;
}

void		ft_is_valid_link(t_game **game, char *line)
{
	char	*first_room;
	char	*second_room;

	first_room = ft_strsub(line, 0,
							ft_strlen(line) - ft_strlen(ft_strchr(line, '-')));
	second_room = ft_strsub(ft_strchr(line, '-'), 1,
							ft_strlen(ft_strchr(line, '-')));
	if (ft_strlen(first_room) == 0 || ft_strlen(second_room) == 0)
		ft_std_error(game, "invalid link", KYEL);
	if (!ft_is_a_room((*game)->rooms, first_room) ||
		!ft_is_a_room((*game)->rooms, second_room))
		ft_std_error(game, "room does not exist", KYEL);
	if (!ft_strcmp(first_room, second_room))
		ft_std_error(game, "room cannot link to itself", KYEL);
	ft_make_link(*game, first_room, second_room);
	free(first_room);
	free(second_room);
}

void		ft_find_start_or_end_helper(t_game **game, t_input **input,
										char *new_line, char *line)
{
	int check;

	if (!new_line)
	{
		free(new_line);
		ft_std_error(game, "error on start/end", KYEL);
	}
	if (!(check = ft_is_valid_room(*game, new_line)))
	{
		free(new_line);
		ft_std_error(game, "error on start/end", KYEL);
	}
	if (check == 2)
		ft_std_error(game, "duplicate rooms -> (start/end) define new rooms",
					KYEL);
	ft_input_push_back(input, ft_new_node(new_line));
	if (!ft_strcmp("##start", line))
		(*game)->start = ft_last_room_added((*game)->rooms);
	else
		(*game)->end = ft_last_room_added((*game)->rooms);
}
