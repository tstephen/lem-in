# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tstephen <tstephen@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/08/22 15:10:08 by tstephen          #+#    #+#              #
#    Updated: 2018/09/03 13:49:45 by tstephen         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = lem-in

L_PATH = ./srcs/

L_SOURCES = input_handler.c \
			main.c \
			utility.c \
			game_handler.c \
			get_rooms.c \
			error_handler.c \
			ant_error_handler.c \
			rooms_error_handler.c \
			links_error_handler.c \
			path_finding.c \
			path_handler.c

L_TO_SOURCES = $(addprefix $(L_PATH), $(L_SOURCES))

L_OBJECTS = $(L_SOURCES: .c=.o)

C_FLAGS = -Wall -Wextra -Werror -g

$(NAME):
	@make -C ./libft/
	@make clean -C ./libft/
	@gcc $(C_FLAGS) -o $(NAME) $(L_TO_SOURCES) ./libft/libft.a 
	
all: $(NAME)

clean:
	@/bin/rm -rf $(L_OBJECTS)

fclean: clean
	@/bin/rm -rf $(NAME)
	@/bin/rm -rf ./libft/libft.a
	@/bin/rm -rf *.dSYM

re: fclean all
