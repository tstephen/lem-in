/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/28 22:39:16 by tstephen          #+#    #+#             */
/*   Updated: 2018/05/30 08:23:27 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_realloc(void *buf, size_t size)
{
	unsigned char	*ptr_buf;
	unsigned char	*ptr_new_mem;
	void			*new_mem;
	size_t			i;

	i = 0;
	if (!(new_mem = (void*)ft_memalloc(size)))
		return (NULL);
	if (!buf)
		return (new_mem);
	ptr_buf = (unsigned char*)buf;
	ptr_new_mem = (unsigned char*)new_mem;
	while (i < size && ptr_buf)
	{
		*ptr_new_mem = *ptr_buf;
		ptr_new_mem++;
		ptr_buf++;
		i++;
	}
	free(buf);
	return (new_mem);
}
