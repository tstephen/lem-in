/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/28 10:51:24 by tstephen          #+#    #+#             */
/*   Updated: 2018/05/25 13:25:41 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnew(size_t size)
{
	char				*new_str;
	unsigned	int		i;

	new_str = (char*)malloc(sizeof(*new_str) * size + 1);
	if (!new_str)
		return (NULL);
	i = 0;
	while (i < size + 1)
	{
		new_str[i] = '\0';
		i++;
	}
	return (new_str);
}
