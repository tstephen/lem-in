/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/09 13:02:18 by tstephen          #+#    #+#             */
/*   Updated: 2018/05/25 13:24:45 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memalloc(size_t size)
{
	void			*result;
	size_t			i;
	unsigned char	*temp;

	i = 0;
	if (!(result = (void*)malloc(size)))
		return (result);
	temp = (unsigned char*)result;
	while (i < size)
	{
		*temp = 0;
		temp++;
		i++;
	}
	return (result);
}
