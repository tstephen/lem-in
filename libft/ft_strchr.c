/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tstephen <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/16 20:20:13 by tstephen          #+#    #+#             */
/*   Updated: 2018/05/20 12:17:23 by tstephen         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *s, int c)
{
	char	*ptr;

	ptr = (char*)s;
	if (*ptr == (char)c)
		return (ptr);
	while (*ptr)
	{
		ptr++;
		if (*ptr == (char)c)
			return (ptr);
	}
	return (NULL);
}
